package com.rave.simplecalculator

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import java.math.BigDecimal

class MainViewModelTest {

    private val mainViewModel = MainViewModel()

    @Test
    @DisplayName("Test addition functionality")
    fun addTest() {
        //Given
        val num1 = 25
        val num2 = 10
        val expression = "+"
        //When
        mainViewModel.calculation(num1,num2,expression)
        //Then
        Assertions.assertEquals(35.0, mainViewModel.result.value)
    }

    @Test
    @DisplayName("Test subtraction functionality")
    fun subtractTest() {
        //Given
        val num1 = 25
        val num2 = 10
        val expression = "-"
        //When
        mainViewModel.calculation(num1,num2,expression)
        //Then
        Assertions.assertEquals(15.0, mainViewModel.result.value)
    }

    @Test
    @DisplayName("Test multiply functionality")
    fun multiplyTest() {
        //Given
        val num1 = 4
        val num2 = 4
        val expression = "*"
        //When
        mainViewModel.calculation(num1,num2,expression)
        //Then
        Assertions.assertEquals(16.0, mainViewModel.result.value)
    }

    @Test
    @DisplayName("Test divide functionality")
    fun divideTest() {
        //Given
        val num1 = 2
        val num2 = 8
        val expression = "/"
        //When
        mainViewModel.calculation(num1,num2,expression)
        //Then
        Assertions.assertEquals(0.25, mainViewModel.result.value)
    }

    @Test
    @DisplayName("Test exponent functionality")
    fun exponentTest() {
        //Given
        val num1 = 2
        val num2 = 3
        val expression = "^"
        //When
        mainViewModel.calculation(num1,num2,expression)
        //Then
        Assertions.assertEquals(8.0, mainViewModel.result.value)
    }
}