package com.rave.simplecalculator

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import java.math.BigDecimal
import kotlin.math.pow

class MainViewModel : ViewModel() {

    private val _result = MutableStateFlow(0.00)
    val result: StateFlow<Double> get() = _result

    fun calculation (num1: Int, num2: Int, exp: String) {
        if (exp === "+") {
            _result.value = (num1 + num2).toDouble()
        } else if (exp === "-") {
            _result.value = (num1 - num2).toDouble()
        } else if (exp === "*") {
            _result.value = (num1 * num2).toDouble()
        } else if (exp === "/") {
            val num = num1.toDouble() / num2
            _result.value = num
        } else if (exp === "^") {
            _result.value = (num1.toDouble().pow(num2))
        } else {
            _result.value = 0.00
        }
    }
}