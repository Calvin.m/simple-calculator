package com.rave.simplecalculator

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CalculatorScreen(
    mainViewModel: MainViewModel,
    result: Double
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
//        verticalArrangement = Arrangement.Center
    ) {
        var val1 by remember { mutableStateOf("") }
        var val2 by remember { mutableStateOf("") }
        var expanded by remember { mutableStateOf(false) }

        OutlinedTextField(
            value = val1,
            onValueChange = { newVal -> val1 = newVal },
            label = { Text(text = "First Number") }
        )
        val symbols = listOf("+", "-", "*", "/", "^")
        var selectedSymbol by remember { mutableStateOf(0) }
        Box(
            modifier = Modifier
                .clickable(onClick = { expanded = true })
                .border(
                    BorderStroke(2.dp, SolidColor(Color.Blue))
                )
        ) {
            Text(
                symbols[selectedSymbol],
                fontSize = 42.sp,
                modifier = Modifier.padding(4.dp)
            )
            DropdownMenu(
                expanded = expanded,
                onDismissRequest = { expanded = false },
                modifier = Modifier
//                .fillMaxWidth()
                    .background(
                        Color.LightGray
                    )
            ) {
                symbols.forEachIndexed { index, symbol ->
                    DropdownMenuItem(
                        enabled = true,
                        text = {
                            Text(
                                text = symbol,
                                fontSize = 36.sp,
                                textAlign = TextAlign.Center
                            )
                        },
                        onClick = {
                            selectedSymbol = index
                            expanded = false
                        },
                    )
                }
            }
        }
//        DropdownMenu(
//            expanded = expanded,
//            onDismissRequest = { expanded = false },
//            modifier = Modifier
////                .fillMaxWidth()
//                .background(
//                    Color.LightGray
//                )
//        ) {
//            symbols.forEachIndexed { index, symbol ->
//                DropdownMenuItem(
//                    enabled = true,
//                    text = {
//                        Text(
//                            text = symbol,
//                            fontSize = 36.sp,
//                            textAlign = TextAlign.Center
//                        )
//                    },
//                    onClick = {
//                        selectedSymbol = index
//                        expanded = false
//                    },
//                )
//            }
//        }

        OutlinedTextField(
            value = val2,
            onValueChange = { newVal -> val2 = newVal },
            label = { Text(text = "Second Number") }
        )

        Button(
            onClick = {
                mainViewModel.calculation(
                    val1.toInt(),
                    val2.toInt(),
                    symbols[selectedSymbol]
                )
            }
        ) {
            Text(text = "Calculate")
        }
        Text(text = "${result}", fontSize = 34.sp)
    }
}